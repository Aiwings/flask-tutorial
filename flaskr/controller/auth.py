from flask import Blueprint,flash, redirect,render_template,request, url_for
from flask_login import current_user, login_user
#from flaskr import init_db;

from flaskr import db
from flaskr.model.user import User
from flaskr.forms import RegisterForm, LoginForm;
bp_auth = Blueprint('auth',__name__, url_prefix='/auth')

@bp_auth.route('/register', methods=('GET','POST'))
def register():
    err =False
    form = RegisterForm()
    if (request.method == 'POST'):
        email = request.form["email"]
        password = request.form["password"]

        if(form.validate() == False):
            flash('All fields are required.',"danger")
        else:
            # Crée une instance de Model User via SQLAlchemy  
            user = User(email = email) 
            user.set_password(password)
            db.session.add(user)
            db.session.commit()
            flash(u"L'inscription s'est déroulée sans encombre", "success")
            redirect(url_for("auth.login"))
    return render_template('auth/register.html',form = form)


@bp_auth.route('/login', methods=('GET','POST'))
def login():
    if current_user.is_authenticated:
        return redirect('/panel/admin')
    error= False
    form = LoginForm()
    if(request.method == 'POST'):
        email = request.form["email"]
        password = request.form["password"]
        if(not(email)):
            flash("Veuillez saisir votre email","danger")
        if(not(password)):
            flash("Veuillez saisir votre mot de passe","danger")
        #Recherche  si l'email est déjç répertorié     
        user =  User.query.filter_by(email=email,).first()
        if( user is not None):
            if(user.check_password(password)):
                flash("Vous êtes bien connecté","success")
                login_user(user)
                return redirect('/panel/admin')
            else:
                flash("Mot de passe incorrect","danger")
        else: 
            flash("L'utlisateur n'existe pas","danger")

    return render_template('auth/login.html',form=form)
