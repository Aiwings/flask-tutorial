from flask import Blueprint,flash, redirect,render_template,request, url_for
from flaskr.model.user import User
from flaskr import db
from flask_login import login_required

#from flaskr.db import get_db
import datetime
bp_panel = Blueprint('panel',__name__, url_prefix='/panel')

@bp_panel.route('/admin', methods=('GET','POST'))
@login_required
def admin():
    #db = get_db()
    #users = db.execute("SELECT email,password FROM user").fetchall()
    users = User.query.all()
    return render_template("panel/panel_admin.html", len = len(users), users = users) 