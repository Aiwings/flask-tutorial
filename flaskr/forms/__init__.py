from flask_wtf import Form
from wtforms import TextField,PasswordField,SubmitField

from wtforms import validators, ValidationError

import re;

class RegisterForm(Form):
    email = TextField(
        "Adresse Email", 
        [
            validators.Required("Veuillez saisir votre adresse email"),
            validators.Email("Veuillez saisir une adresse E-mail valide")
        ]
    )
    password = PasswordField("Mot de passe",
        [
            validators.Required("Veuillez saisir votre mot de passe"),
            validators.Regexp("[A-Za-z0-9]{8,}",message="7 caractères : chiffres, minuscules, majuscules")
        ]
    )
    
    password_conf = PasswordField("Confirmez le mot de passe",
        [
            validators.Required("Confirmez votre mot de passe"),
            validators.equal_to('password',message="Les mots de passes doivent être identiques" )
        ]
    )
    submit = SubmitField("S'enregistrer")

class LoginForm(Form):
    email = TextField(
        "Adresse Email", 
        [
            validators.Required("Veuillez saisir votre adresse email"),
            validators.Email("Veuillez saisir une adresse E-mail valide")
        ]
    )
    password = PasswordField("Mot de passe",
        [
            validators.Required("Veuillez saisir votre mot de passe")
        ]
    )
    submit = SubmitField("Se connecter")