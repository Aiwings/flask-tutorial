from flaskr import db
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
# Modèle Flask-User, basé sur SQL Alchemy
class User (db.Model, UserMixin):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.Unicode(255), nullable=False, server_default=u'', unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')
    active = db.Column(db.Boolean(), nullable=False, server_default='0')
    first_name = db.Column(db.Unicode(50), nullable=True, server_default=u'')
    last_name = db.Column(db.Unicode(50), nullable=True, server_default=u'')
    def __repr__(self):
        return '<User %r>' % self.username
    
    def set_password(self,pw):
        self.password = generate_password_hash(pw)
     
    def check_password(self,pw):
        return check_password_hash(self.password,pw)
