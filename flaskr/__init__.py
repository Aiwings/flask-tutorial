import os
from flask import Flask, redirect

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager


# Database Managed by SQLAlchemy
db = SQLAlchemy();
login = LoginManager()



def create_app(test_config=None):
    
    #Configuration de l'App Flask
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
        SQLALCHEMY_DATABASE_URI ="sqlite:////"+ os.path.join(app.instance_path, 'flaskr.sqlite'),
    )
    db.init_app(app)
    login.init_app(app)
    login.login_view = '/auth/login'

    if test_config is None:
        # load the instance config, if it exists, when nfot testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello

    from .model.user import User;
    @app.before_first_request
    def create_all():
        db.create_all()
    @login.user_loader
    def load_user(id):
        return User.query.get(int(id))

    @app.route("/")
    def home():
        redirect("/panel/admin")
    
    from .controller import auth, panel;
    app.register_blueprint(auth.bp_auth)
    app.register_blueprint(panel.bp_panel)
    return app